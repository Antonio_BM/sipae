import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PremiosReconocimientosService {

  url = 'http://localhost:8000/api/reconocimientos';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarPremiosReconocimientos(premios_reconocimientos: JSON){
  	return this.http.post(this.url,JSON.stringify(premios_reconocimientos));
  }

  eliminarPremiosReconocimientos(premios_reconocimientos: JSON){
  	return this.http.delete(`${this.url}/${premios_reconocimientos['id_reconocimiento']}`)
  }
}
