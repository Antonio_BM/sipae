import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductosAcademicosService {

  url = 'http://localhost:8000/api/productosacademicos';

  constructor(private http: HttpClient) { }

  llenarCatalogos(){
  	return this.http.get(this.url)
  }

  insertarProductoAcademico(productos_academicos: JSON){
  	return this.http.post(this.url,JSON.stringify(productos_academicos));
  }

  eliminarProductoAcademico(productos_academicos: JSON){
  	return this.http.delete(`${this.url}/${productos_academicos['id_producto_academico']}`)
  }
}
