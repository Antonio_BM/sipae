import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CapacitacionDocenteService {

  url = 'http://localhost:8000/api/capacitaciondocentes';

  constructor(private http: HttpClient) { }

  insertarCapacitacionDocente(capacitacion: JSON){
  	return this.http.post(this.url,JSON.stringify(capacitacion));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarCapacitacionDocente(capacitacion:JSON){
  	return this.http.delete(`${this.url}/${capacitacion['id_capacitacion_docente']}`)
  }

  actualizarCapacitacionDocente(capacitacion: JSON){
    return this.http.put(`${this.url}/${capacitacion['id_capacitacion_docente']}`,
      JSON.stringify(capacitacion))
  }
}
