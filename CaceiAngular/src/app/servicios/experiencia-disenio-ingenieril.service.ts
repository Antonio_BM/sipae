import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExperienciaDisenioIngenierilService {

  url = 'http://localhost:8000/api/experienciadisenioingenieril';

  constructor(private http: HttpClient) { }

  insertarExperienciaDisenioIngenieril(experiencia_disenio_ingenieril: JSON){
  	return this.http.post(this.url,JSON.stringify(experiencia_disenio_ingenieril));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarExperienciaDisenioIngenieril(experiencia_disenio_ingenieril: JSON){
  	return this.http.delete(
  		`${this.url}/${experiencia_disenio_ingenieril['id_experiencia_disenio_ingenieril']}`
  	)
  }
}
