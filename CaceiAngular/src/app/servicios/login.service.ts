import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = 'http://localhost:8000/api/login';
  private datosUsuario :JSON;

  constructor(private http: HttpClient) {}

  iniciarSesion(usuario: JSON){
  	return this.http.post(this.url,JSON.stringify(usuario));
  }

  verificarSesion(){
    this.datosUsuario = JSON.parse(localStorage.getItem("usuario"));
  	/*if(this.datosUsuario == null){
    }*/
  	//console.log(this.datosUsuario);
  	return this.http.post(this.url+"/verificarsesion",JSON.stringify(this.datosUsuario));
  }

  cerrarSesion(){
  	localStorage.removeItem("usuario");
  }

}
