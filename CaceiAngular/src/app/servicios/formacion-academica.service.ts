import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormacionAcademicaService {

  url = 'http://localhost:8000/api/formacionacademica';

  constructor(private http: HttpClient) { }

  insertarFormacionAcademica(formacion: JSON){
  	return this.http.post(this.url,JSON.stringify(formacion));
  }

  llenarCatalogos() {
  	return this.http.get(this.url);
  }

  eliminarFormacionAcademica(formacion: JSON){
  	return this.http.delete(`${this.url}/${formacion['id_formacion_academica']}`)
  }
  actualizarFormacionAcademica(formacion: JSON){
    return this.http.put(`${this.url}/${formacion['id_formacion_academica']}`,
      JSON.stringify(formacion))
  }
}
