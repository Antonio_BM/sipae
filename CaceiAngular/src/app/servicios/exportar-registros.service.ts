import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExportarRegistrosService {

	url = 'http://localhost:8000/api/exportardatos';

  constructor(private http: HttpClient) { }

  descargarExcel(){
  	return this.http.get(this.url,{responseType:'blob'})
  }
}
