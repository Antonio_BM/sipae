import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource  } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { ParticipacionesOrgProfesionalesService } from '../servicios/participaciones-org-profesionales.service'; 

@Component ({
  selector: 'app-participaciones-org-profesionales',
  templateUrl: './participaciones-org-profesionales.component.html',
  styleUrls: ['./participaciones-org-profesionales.component.css']
})
export class ParticipacionesOrgProfesionalesComponent implements OnInit {

  modal;
  participaciones_org_profesionales:JSON;
  niveles_participacion_org: any = [];
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id', 'organismo', 'accion']

  constructor(private modalService: NgbModal, private _service: ParticipacionesOrgProfesionalesService) {}

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  borrarRegistro(element){
    this._service.eliminarParticipacionesOrgProfesionales(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.participaciones_org_profesionales = catalogos['paises'];
        this.niveles_participacion_org = catalogos['niveles_participacion_org'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(catalogos['registros']);
      }
    );
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm) {
  	formulario.reset;
    this.participaciones_org_profesionales = formulario.value;
    this.participaciones_org_profesionales['id_usuario'] = 1;
    this._service.insertarParticipacionesOrgProfesionales(this.participaciones_org_profesionales).
    subscribe(
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      }
    )
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator
        console.log(this.registros);
      }
    )
  	this.modal.close();
  }

}
