import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { LoginService } from './../servicios/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _service: LoginService, private router: Router) { }

  private usuario: JSON;

  ngOnInit(): void {
  	this._service.verificarSesion().subscribe(
  			datos => {
  				if(datos['logged']){
  					this.router.navigate(['informacion-general']);
  				}
  			}
  		);
  }

  iniciarSesion(formulario:NgForm){
  	this.usuario = formulario.value
  	this._service.iniciarSesion(this.usuario).subscribe(
  			datos => {
  				if(datos['logged']){
  					localStorage.setItem("usuario",JSON.stringify(datos));
            //console.log(this);
			  		this.router.navigate(['/informacion-general']);
			  	}
  			}, error => {
          alert("Usuario o contraseña incorrectos");
        }
  		);
  }

}
