import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CapacitacionDocenteComponent } from './capacitacion-docente.component';

describe('CapacitacionDocenteComponent', () => {
  let component: CapacitacionDocenteComponent;
  let fixture: ComponentFixture<CapacitacionDocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacitacionDocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacitacionDocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
