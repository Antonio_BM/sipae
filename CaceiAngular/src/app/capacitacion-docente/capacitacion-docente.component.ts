import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { CapacitacionDocenteService } from './../servicios/capacitacion-docente.service';

@Component ({
  selector: 'app-capacitacion-docente',
  templateUrl: './capacitacion-docente.component.html',
  styleUrls: ['./capacitacion-docente.component.css']
})
export class CapacitacionDocenteComponent implements OnInit {

  modal;
  capacitacion:JSON;
  paises: any = [];
  niveles_capacitacion_docente: any = [];
  registros: MatTableDataSource<JSON> = null;
  capacitacionEditar: JSON
  actualizar: boolean = false


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id','tipo capacitacion','accion'];

  constructor(private modalService: NgbModal, private _service: CapacitacionDocenteService) {}

  ngOnInit(): void {
    this.llenarCatalogos();
  }

  llenarCatalogos() {
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.paises = catalogos['paises'];
        this.niveles_capacitacion_docente = catalogos['niveles_capacitacion_docente'];
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
        //console.log(this.registros);
      }
    );
  }

  agregarRegistro(content) {
    this.actualizar = false
    this.capacitacionEditar = JSON.parse("{}")
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  borrarRegistro(element){
    this._service.eliminarCapacitacionDocente(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  editarRegistro(content,element){
    this.actualizar = true
    this.capacitacionEditar = element
    this.modal = this.modalService.open(content, {size: 'lg'})
  }

  editar(formulario: NgForm){
    this.capacitacion = formulario.value
    this.capacitacion['id_capacitacion_docente'] = 
      this.capacitacionEditar['id_capacitacion_docente']
    this.registros.data[this.registros.data.indexOf(this.capacitacionEditar)] =
      this.capacitacion
     this.registros._updateChangeSubscription()
     this._service.actualizarCapacitacionDocente(this.capacitacion).subscribe()
     this.cerrar()
  }

  cerrar() {
  	this.modal.close();
  }

  guardar(formulario:NgForm) {
  	formulario.reset;
    this.capacitacion = formulario.value;
    this.capacitacion['id_usuario'] = 1;
    this._service.insertarCapacitacionDocente(this.capacitacion).subscribe(
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      } 
    );
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
      }
    )
  	console.log(this.capacitacion);
  	this.modal.close();
  }

}
