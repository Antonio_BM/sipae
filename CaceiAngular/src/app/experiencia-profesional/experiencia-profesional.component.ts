import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ExperienciaProfesionalService } from '../servicios/experiencia-profesional.service';

@Component({
  selector: 'app-experiencia-profesional',
  templateUrl: './experiencia-profesional.component.html',
  styleUrls: ['./experiencia-profesional.component.css']
})
export class ExperienciaProfesionalComponent implements OnInit {

  modal;
  experiencia_profesional:JSON;
  registros = null

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ["id", "actividad", "accion"]

  constructor(private modalService: NgbModal, private _service: ExperienciaProfesionalService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarExperienciaProfesional(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros'])
        this.registros.paginator = this.paginator
        console.log(catalogos['registros'])
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.experiencia_profesional = formulario.value;
    this.experiencia_profesional["id_usuario"] = 1;
    this._service.insertarExperienciaProfesional(this.experiencia_profesional).subscribe(
      (respuesta: any) => {
        alert(respuesta.mensaje)
      }
    )
    this.llenarCatalogos()
  	console.log(this.experiencia_profesional);
  	this.modal.close();
  }

}
