import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './../servicios/login.service'; 
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private permisos: boolean;

  constructor(private _service:LoginService, private router: Router){}

  canActivate(
  next: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      return new Observable<boolean>( // creamos el observable de tipo bool
        obs => this._service.verificarSesion().subscribe( // llamamos el servicio
            data => {
              obs.next(true)
            }, error => {
              console.log(error)
               this.router.navigate(['/login']); // redireccion en caso de que no este el usuario
               alert("Debe iniciar sesión antes.");
              obs.next(false)
            }
          )  
      )
  }
}
