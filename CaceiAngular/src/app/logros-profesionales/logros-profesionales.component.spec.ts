import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LogrosProfesionalesComponent } from './logros-profesionales.component';

describe('LogrosProfesionalesComponent', () => {
  let component: LogrosProfesionalesComponent;
  let fixture: ComponentFixture<LogrosProfesionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogrosProfesionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogrosProfesionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
