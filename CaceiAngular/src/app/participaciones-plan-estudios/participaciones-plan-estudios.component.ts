import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { ParticipacionesPlanEstudiosService } from '../servicios/participaciones-plan-estudios.service';

@Component({
  selector: 'app-participaciones-plan-estudios',
  templateUrl: './participaciones-plan-estudios.component.html',
  styleUrls: ['./participaciones-plan-estudios.component.css'] 
})
export class ParticipacionesPlanEstudiosComponent implements OnInit {

  modal;
  participaciones_plan_estudios:JSON;
  registros = null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  columnas: string[] = ['id', 'descripcion', 'accion']

  constructor(private modalService: NgbModal, private _service: ParticipacionesPlanEstudiosService) { }

  ngOnInit(): void {
    this.llenarCatalogos()
  }

  borrarRegistro(element){
    this._service.eliminarParticipacionesPlanEstudios(element).subscribe()
    const index = this.registros.data.indexOf(element)
    this.registros.data.splice(index, 1)
    this.registros._updateChangeSubscription()
  }

  llenarCatalogos(){
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
      }
    )
  }

  agregarRegistro(content) {
    this.modal = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', 
      size: 'lg'});
  }

  cerrar(){
  	this.modal.close();
  }

  guardar(formulario:NgForm){
  	formulario.reset;
    this.participaciones_plan_estudios = formulario.value;
    this.participaciones_plan_estudios['id_usuario'] = 1;
    this._service.insertarParticipacionesPlanEstudios(this.participaciones_plan_estudios).subscribe(
      (respuesta: any) => {
        alert(respuesta['mensaje'])
      } 
    )
    this._service.llenarCatalogos().subscribe(
      catalogos => {
        this.registros = new MatTableDataSource<JSON>(catalogos['registros']);
        this.registros.paginator = this.paginator;
        console.log(catalogos['registros']);
      }
    )
  	console.log(this.participaciones_plan_estudios);
  	this.modal.close();
  }

}
