-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: Cacei
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actualizaciones_disciplinares`
--

DROP TABLE IF EXISTS `actualizaciones_disciplinares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actualizaciones_disciplinares` (
  `id_actualizacion_disciplinar` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_actualizacion` varchar(30) NOT NULL,
  `institucion_actualizacion` varchar(40) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `fecha_actualizacion` date NOT NULL,
  `horas_actualizacion` decimal(3,0) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_actualizacion_disciplinar`),
  KEY `id_pais_paises_actualizaciones_disciplinares_fk` (`id_pais`),
  KEY `id_usaurio_usuarios_actualizaciones_disciplinares_fk` (`id_usuario`),
  CONSTRAINT `id_pais_paises_actualizaciones_disciplinares_fk` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id_pais`),
  CONSTRAINT `id_usaurio_usuarios_actualizaciones_disciplinares_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actualizaciones_disciplinares`
--

LOCK TABLES `actualizaciones_disciplinares` WRITE;
/*!40000 ALTER TABLE `actualizaciones_disciplinares` DISABLE KEYS */;
INSERT INTO `actualizaciones_disciplinares` VALUES (1,'Escolar','UPQ',5,'2002-02-20',800,1,NULL,NULL,NULL),(2,'prod','UAQ',10,'2006-08-20',200,1,NULL,NULL,NULL),(3,'prod2','UNAM',10,'2006-08-20',200,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `actualizaciones_disciplinares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capacitaciones_docentes`
--

DROP TABLE IF EXISTS `capacitaciones_docentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capacitaciones_docentes` (
  `id_capacitacion_docente` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_capacitacion` varchar(30) NOT NULL,
  `institucion_capacitacion` varchar(40) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `fecha_capacitacion` date NOT NULL,
  `horas_capacitacion` decimal(3,0) NOT NULL,
  `id_nivel_capacitacion_docente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_capacitacion_docente`),
  KEY `id_usuario_usuarios_capacitaciones_docentes` (`id_usuario`),
  KEY `id_pais_paises_capacitaciones_docentes_fk` (`id_pais`),
  KEY `id_nivel_cap_nivel_cap_cap_docentes_fk` (`id_nivel_capacitacion_docente`),
  CONSTRAINT `id_nivel_cap_nivel_cap_cap_docentes_fk` FOREIGN KEY (`id_nivel_capacitacion_docente`) REFERENCES `niveles_capacitacion_docente` (`id_nivel_capacitacion_docente`),
  CONSTRAINT `id_pais_paises_capacitaciones_docentes_fk` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id_pais`),
  CONSTRAINT `id_usuario_usuarios_capacitaciones_docentes` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitaciones_docentes`
--

LOCK TABLES `capacitaciones_docentes` WRITE;
/*!40000 ALTER TABLE `capacitaciones_docentes` DISABLE KEYS */;
INSERT INTO `capacitaciones_docentes` VALUES (1,'Enseñanza','UAQ',2,'2006-03-15',500,3,1,NULL,NULL,NULL),(2,'Laravel','UVM',6,'2005-03-15',250,2,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `capacitaciones_docentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claves_cursos`
--

DROP TABLE IF EXISTS `claves_cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claves_cursos` (
  `id_clave_curso` int(11) NOT NULL AUTO_INCREMENT,
  `clave_curso` varchar(10) NOT NULL,
  PRIMARY KEY (`id_clave_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claves_cursos`
--

LOCK TABLES `claves_cursos` WRITE;
/*!40000 ALTER TABLE `claves_cursos` DISABLE KEYS */;
INSERT INTO `claves_cursos` VALUES (1,'ECO'),(2,'OTR');
/*!40000 ALTER TABLE `claves_cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos_impartidos`
--

DROP TABLE IF EXISTS `cursos_impartidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos_impartidos` (
  `id_cursos_impartidos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_curso_impartido` varchar(40) NOT NULL,
  `fecha_inicio_curso_impartido` date NOT NULL,
  `fecha_fin_curso_impartido` date NOT NULL,
  `id_clave_curso` int(11) NOT NULL,
  `id_tipo_curso` int(11) NOT NULL,
  `horas_curso_impartido` decimal(4,0) NOT NULL,
  `evaluacion_desempenio` decimal(3,0) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` date DEFAULT NULL,
  PRIMARY KEY (`id_cursos_impartidos`),
  KEY `id_clave_curso_claves_cursos_cursos_impartidos_fk` (`id_clave_curso`),
  KEY `id_tipo_curso_tipos_cursos_cursos_impartidos_fk` (`id_tipo_curso`),
  KEY `id_usuario_usuarios_cursos_impartidos_fk` (`id_usuario`),
  CONSTRAINT `id_clave_curso_claves_cursos_cursos_impartidos_fk` FOREIGN KEY (`id_clave_curso`) REFERENCES `claves_cursos` (`id_clave_curso`),
  CONSTRAINT `id_tipo_curso_tipos_cursos_cursos_impartidos_fk` FOREIGN KEY (`id_tipo_curso`) REFERENCES `tipos_cursos` (`id_tipo_curso`),
  CONSTRAINT `id_usuario_usuarios_cursos_impartidos_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos_impartidos`
--

LOCK TABLES `cursos_impartidos` WRITE;
/*!40000 ALTER TABLE `cursos_impartidos` DISABLE KEYS */;
INSERT INTO `cursos_impartidos` VALUES (1,'Poo','2012-03-15','2012-05-14',1,1,505,100,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cursos_impartidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencias_disenio_ingenieril`
--

DROP TABLE IF EXISTS `experiencias_disenio_ingenieril`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencias_disenio_ingenieril` (
  `id_experiencia_disenio_ingenieril` int(11) NOT NULL AUTO_INCREMENT,
  `organismo_exp_ingenieril` varchar(40) NOT NULL,
  `fecha_inicio_exp_ingenieril` date NOT NULL,
  `fecha_fin_exp_ingenieril` date NOT NULL,
  `id_nivel_experiencia_ingenieril` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_experiencia_disenio_ingenieril`),
  KEY `id_usuario_usuarios_expriencias_disenio_ingenieril_fk` (`id_usuario`),
  KEY `id_nivel_exp_ing_niveles_exp_ing_exp_disenio_ing` (`id_nivel_experiencia_ingenieril`),
  CONSTRAINT `id_nivel_exp_ing_niveles_exp_ing_exp_disenio_ing` FOREIGN KEY (`id_nivel_experiencia_ingenieril`) REFERENCES `niveles_experiencia_ingenieril` (`id_nivel_experiencia_ingenieril`),
  CONSTRAINT `id_usuario_usuarios_expriencias_disenio_ingenieril_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencias_disenio_ingenieril`
--

LOCK TABLES `experiencias_disenio_ingenieril` WRITE;
/*!40000 ALTER TABLE `experiencias_disenio_ingenieril` DISABLE KEYS */;
/*!40000 ALTER TABLE `experiencias_disenio_ingenieril` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencias_profesionales`
--

DROP TABLE IF EXISTS `experiencias_profesionales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencias_profesionales` (
  `id_experiencias_profesionales` int(11) NOT NULL AUTO_INCREMENT,
  `actividad_puesto_experiencia` varchar(40) NOT NULL,
  `empresa_experiencia` varchar(40) NOT NULL,
  `fecha_inicio_experiencia` date NOT NULL,
  `fecha_fin_experiencia` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_experiencias_profesionales`),
  KEY `id_usuario_usuarios_experiencias_profesionales` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_experiencias_profesionales` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencias_profesionales`
--

LOCK TABLES `experiencias_profesionales` WRITE;
/*!40000 ALTER TABLE `experiencias_profesionales` DISABLE KEYS */;
/*!40000 ALTER TABLE `experiencias_profesionales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formacion_academica`
--

DROP TABLE IF EXISTS `formacion_academica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formacion_academica` (
  `id_formacion_academica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_nivel_academico` varchar(50) NOT NULL,
  `institucion_formacion` varchar(50) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `estado_formacion` varchar(40) NOT NULL,
  `ciudad_formacion` varchar(40) NOT NULL,
  `fecha_formacion` date NOT NULL,
  `cedula_profesional` decimal(8,0) NOT NULL,
  `id_tipo_formacion_academica` int(11) NOT NULL,
  `id_tipo_localidad` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_formacion_academica`),
  KEY `id_usuario_usuarios_formacion_academica_fk` (`id_usuario`),
  KEY `id_pais_paises_formacion_academica_fk` (`id_pais`),
  KEY `id_tipo_localidad_tipos_localidad_formacion_academica_fk` (`id_tipo_localidad`),
  KEY `id_tipo_form_academica_tipos_form_academica_form_academica_fk` (`id_tipo_formacion_academica`),
  CONSTRAINT `id_pais_paises_formacion_academica_fk` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id_pais`),
  CONSTRAINT `id_tipo_form_academica_tipos_form_academica_form_academica_fk` FOREIGN KEY (`id_tipo_formacion_academica`) REFERENCES `tipos_formacion_academica` (`id_tipo_formacion_academica`),
  CONSTRAINT `id_tipo_localidad_tipos_localidad_formacion_academica_fk` FOREIGN KEY (`id_tipo_localidad`) REFERENCES `tipos_localidad` (`id_tipo_localidad`),
  CONSTRAINT `id_usuario_usuarios_formacion_academica_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formacion_academica`
--

LOCK TABLES `formacion_academica` WRITE;
/*!40000 ALTER TABLE `formacion_academica` DISABLE KEYS */;
INSERT INTO `formacion_academica` VALUES (1,'Licenciatura','ITQ',50,'QRO','Querétaro','2003-05-06',5685,2,2,1,NULL,NULL,NULL),(2,'PROF','IPN',141,'MEX','MEX','2007-05-09',25353,3,1,1,NULL,NULL,NULL),(3,'Ing','UPQ',3,'Qro','Qro','2006-04-11',256,4,3,1,'2020-05-26',NULL,NULL),(4,'Licenciatura','ITQ',141,'Querétaro','Querétaro','2008-02-21',658,1,2,1,'2020-05-26',NULL,NULL),(5,'Ingeniería','UAQ',141,'Querétaro','Querétaro','2006-02-10',985,1,1,1,'2020-05-26',NULL,NULL),(6,'Bueno','ITQ',141,'Queretaro','Queretaro','2020-06-18',5441,1,1,1,'2020-06-09',NULL,NULL),(7,'Maestro','UNAM',20,'QRO','QRO','1990-06-15',563245,3,1,1,NULL,NULL,NULL),(8,'Abodago','UNAM',20,'QRO','QRO','1990-06-15',563245,3,1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `formacion_academica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacion_general`
--

DROP TABLE IF EXISTS `informacion_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacion_general` (
  `id_informacion_general` int(11) NOT NULL AUTO_INCREMENT,
  `numero_profesor` decimal(3,0) NOT NULL,
  `nombres_profesor` varchar(40) NOT NULL,
  `apellido_paterno` varchar(40) NOT NULL,
  `apellido_materno` varchar(40) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `puesto_en_institucion` varchar(40) NOT NULL,
  `fecha_contratacion_institucion` date NOT NULL,
  `categoria_contratacion_actual` varchar(40) NOT NULL,
  `tipo_contratacion_actual` varchar(40) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_informacion_general`),
  KEY `id_usuario_usuarios_informacion_general_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_informacion_general_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacion_general`
--

LOCK TABLES `informacion_general` WRITE;
/*!40000 ALTER TABLE `informacion_general` DISABLE KEYS */;
INSERT INTO `informacion_general` VALUES (1,409,'Antonio','Briseño','Montes','1997-04-09','Programador','2020-03-11','Presidente','Contrato',1,NULL,NULL,NULL),(2,555,'Charlie','Arenas','Elías','1997-03-06','Programador','2020-02-11','Prueba1','Prueba2',1,NULL,NULL,NULL),(3,501,'Fernando','Perez','Lopez','1996-09-05','Diseñador','2019-06-05','Presidente','contrato',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `informacion_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logros_profesionales`
--

DROP TABLE IF EXISTS `logros_profesionales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logros_profesionales` (
  `id_logro_profesional` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_logro_profesional` varchar(250) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_modificacion` date NOT NULL,
  `id_usuario_modifico` int(11) NOT NULL,
  PRIMARY KEY (`id_logro_profesional`),
  KEY `id_usuario_usuarios_logros_profesionales_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_logros_profesionales_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logros_profesionales`
--

LOCK TABLES `logros_profesionales` WRITE;
/*!40000 ALTER TABLE `logros_profesionales` DISABLE KEYS */;
/*!40000 ALTER TABLE `logros_profesionales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `niveles_capacitacion_docente`
--

DROP TABLE IF EXISTS `niveles_capacitacion_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveles_capacitacion_docente` (
  `id_nivel_capacitacion_docente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_nivel_capacitacion_docente` varchar(20) NOT NULL,
  PRIMARY KEY (`id_nivel_capacitacion_docente`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `niveles_capacitacion_docente`
--

LOCK TABLES `niveles_capacitacion_docente` WRITE;
/*!40000 ALTER TABLE `niveles_capacitacion_docente` DISABLE KEYS */;
INSERT INTO `niveles_capacitacion_docente` VALUES (1,'Nula'),(2,'Poca'),(3,'Regular'),(4,'Suficiente'),(5,'Amplia');
/*!40000 ALTER TABLE `niveles_capacitacion_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `niveles_experiencia_ingenieril`
--

DROP TABLE IF EXISTS `niveles_experiencia_ingenieril`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveles_experiencia_ingenieril` (
  `id_nivel_experiencia_ingenieril` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_nivel_experiencia_ingenieril` varchar(20) NOT NULL,
  PRIMARY KEY (`id_nivel_experiencia_ingenieril`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `niveles_experiencia_ingenieril`
--

LOCK TABLES `niveles_experiencia_ingenieril` WRITE;
/*!40000 ALTER TABLE `niveles_experiencia_ingenieril` DISABLE KEYS */;
INSERT INTO `niveles_experiencia_ingenieril` VALUES (1,'Nulo'),(2,'Poco'),(3,'Regular'),(4,'Suficiente'),(5,'Amplio');
/*!40000 ALTER TABLE `niveles_experiencia_ingenieril` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `niveles_participacion_org`
--

DROP TABLE IF EXISTS `niveles_participacion_org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveles_participacion_org` (
  `id_nivel_participacion_org` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_nivel_participacion_org` varchar(20) NOT NULL,
  PRIMARY KEY (`id_nivel_participacion_org`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `niveles_participacion_org`
--

LOCK TABLES `niveles_participacion_org` WRITE;
/*!40000 ALTER TABLE `niveles_participacion_org` DISABLE KEYS */;
/*!40000 ALTER TABLE `niveles_participacion_org` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paises` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_pais` varchar(60) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'Afganistán'),(2,'Albania'),(3,'Alemania'),(4,'Andorra'),(5,'Angola'),(6,'Anguila'),(7,'Antártida'),(8,'Antigua y Barbuda'),(9,'Arabia Saudita'),(10,'Argelia'),(11,'Argentina'),(12,'Armenia'),(13,'Aruba'),(14,'Australia'),(15,'Austria'),(16,'Azerbaiyán'),(17,'Bélgica'),(18,'Bahamas'),(19,'Bahrein'),(20,'Bangladesh'),(21,'Barbados'),(22,'Belice'),(23,'Benín'),(24,'Bhután'),(25,'Bielorrusia'),(26,'Birmania'),(27,'Bolivia'),(28,'Bosnia y Herzegovina'),(29,'Botsuana'),(30,'Brasil'),(31,'Brunéi'),(32,'Bulgaria'),(33,'Burkina Faso'),(34,'Burundi'),(35,'Cabo Verde'),(36,'Camboya'),(37,'Camerún'),(38,'Canadá'),(39,'Chad'),(40,'Chile'),(41,'China'),(42,'Chipre'),(43,'Ciudad del Vaticano'),(44,'Colombia'),(45,'Comoras'),(46,'República del Congo'),(47,'República Democrática del Congo'),(48,'Corea del Norte'),(49,'Corea del Sur'),(50,'Costa de Marfil'),(51,'Costa Rica'),(52,'Croacia'),(53,'Cuba'),(54,'Curazao'),(55,'Dinamarca'),(56,'Dominica'),(57,'Ecuador'),(58,'Egipto'),(59,'El Salvador'),(60,'Emiratos Árabes Unidos'),(61,'Eritrea'),(62,'Eslovaquia'),(63,'Eslovenia'),(64,'España'),(65,'Estados Unidos de América'),(66,'Estonia'),(67,'Etiopía'),(68,'Filipinas'),(69,'Finlandia'),(70,'Fiyi'),(71,'Francia'),(72,'Gabón'),(73,'Gambia'),(74,'Georgia'),(75,'Ghana'),(76,'Gibraltar'),(77,'Granada'),(78,'Grecia'),(79,'Groenlandia'),(80,'Guadalupe'),(81,'Guam'),(82,'Guatemala'),(83,'Guayana Francesa'),(84,'Guernsey'),(85,'Guinea'),(86,'Guinea Ecuatorial'),(87,'Guinea-Bissau'),(88,'Guyana'),(89,'Haití'),(90,'Honduras'),(91,'Hong kong'),(92,'Hungría'),(93,'India'),(94,'Indonesia'),(95,'Irán'),(96,'Irak'),(97,'Irlanda'),(98,'Isla Bouvet'),(99,'Isla de Man'),(100,'Isla de Navidad'),(101,'Isla Norfolk'),(102,'Islandia'),(103,'Islas Bermudas'),(104,'Islas Caimán'),(105,'Islas Cocos (Keeling)'),(106,'Islas Cook'),(107,'Islas de Åland'),(108,'Islas Feroe'),(109,'Islas Georgias del Sur y Sandwich del Sur'),(110,'Islas Heard y McDonald'),(111,'Islas Maldivas'),(112,'Islas Malvinas'),(113,'Islas Marianas del Norte'),(114,'Islas Marshall'),(115,'Islas Pitcairn'),(116,'Islas Salomón'),(117,'Islas Turcas y Caicos'),(118,'Islas Ultramarinas Menores de Estados Unidos'),(119,'Islas Vírgenes Británicas'),(120,'Islas Vírgenes de los Estados Unidos'),(121,'Israel'),(122,'Italia'),(123,'Jamaica'),(124,'Japón'),(125,'Jersey'),(126,'Jordania'),(127,'Kazajistán'),(128,'Kenia'),(129,'Kirguistán'),(130,'Kiribati'),(131,'Kuwait'),(132,'Líbano'),(133,'Laos'),(134,'Lesoto'),(135,'Letonia'),(136,'Liberia'),(137,'Libia'),(138,'Liechtenstein'),(139,'Lituania'),(140,'Luxemburgo'),(141,'México'),(142,'Mónaco'),(143,'Macao'),(144,'Macedônia'),(145,'Madagascar'),(146,'Malasia'),(147,'Malawi'),(148,'Mali'),(149,'Malta'),(150,'Marruecos'),(151,'Martinica'),(152,'Mauricio'),(153,'Mauritania'),(154,'Mayotte'),(155,'Micronesia'),(156,'Moldavia'),(157,'Mongolia'),(158,'Montenegro'),(159,'Montserrat'),(160,'Mozambique'),(161,'Namibia'),(162,'Nauru'),(163,'Nepal'),(164,'Nicaragua'),(165,'Niger'),(166,'Nigeria'),(167,'Niue'),(168,'Noruega'),(169,'Nueva Caledonia'),(170,'Nueva Zelanda'),(171,'Omán'),(172,'Países Bajos'),(173,'Pakistán'),(174,'Palau'),(175,'Palestina'),(176,'Panamá'),(177,'Papúa Nueva Guinea'),(178,'Paraguay'),(179,'Perú'),(180,'Polinesia Francesa'),(181,'Polonia'),(182,'Portugal'),(183,'Puerto Rico'),(184,'Qatar'),(185,'Reino Unido'),(186,'República Centroafricana'),(187,'República Checa'),(188,'República Dominicana'),(189,'República de Sudán del Sur'),(190,'Reunión'),(191,'Ruanda'),(192,'Rumanía'),(193,'Rusia'),(194,'Sahara Occidental'),(195,'Samoa'),(196,'Samoa Americana'),(197,'San Bartolomé'),(198,'San Cristóbal y Nieves'),(199,'San Marino'),(200,'San Martín (Francia)'),(201,'San Pedro y Miquelón'),(202,'San Vicente y las Granadinas'),(203,'Santa Elena'),(204,'Santa Lucía'),(205,'Santo Tomé y Príncipe'),(206,'Senegal'),(207,'Serbia'),(208,'Seychelles'),(209,'Sierra Leona'),(210,'Singapur'),(211,'Sint Maarten'),(212,'Siria'),(213,'Somalia'),(214,'Sri lanka'),(215,'Sudáfrica'),(216,'Sudán'),(217,'Suecia'),(218,'Suiza'),(219,'Surinám'),(220,'Svalbard y Jan Mayen'),(221,'Swazilandia'),(222,'Tayikistán'),(223,'Tailandia'),(224,'Taiwán'),(225,'Tanzania'),(226,'Territorio Británico del Océano Índico'),(227,'Territorios Australes y Antárticas Franceses'),(228,'Timor Oriental'),(229,'Togo'),(230,'Tokelau'),(231,'Tonga'),(232,'Trinidad y Tobago'),(233,'Tunez'),(234,'Turkmenistán'),(235,'Turquía'),(236,'Tuvalu'),(237,'Ucrania'),(238,'Uganda'),(239,'Uruguay'),(240,'Uzbekistán'),(241,'Vanuatu'),(242,'Venezuela'),(243,'Vietnam'),(244,'Wallis y Futuna'),(245,'Yemen'),(246,'Yibuti'),(247,'Zambia'),(248,'Zimbabue');
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participaciones_org_profesionales`
--

DROP TABLE IF EXISTS `participaciones_org_profesionales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participaciones_org_profesionales` (
  `id_participacion_org_profesional` int(11) NOT NULL AUTO_INCREMENT,
  `organismo_participacion` varchar(40) NOT NULL,
  `fecha_inicio_participacion` date NOT NULL,
  `fecha_termino_participacion` date NOT NULL,
  `id_nivel_participacion_org` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_modificacion` date NOT NULL,
  `id_usuario_modifico` int(11) NOT NULL,
  PRIMARY KEY (`id_participacion_org_profesional`),
  KEY `id_usuario_usuarios_participaciones_org_profesionales_fk` (`id_usuario`),
  KEY `id_nivel_part_org_niveles_part_org_part_org_profesionales` (`id_nivel_participacion_org`),
  CONSTRAINT `id_nivel_part_org_niveles_part_org_part_org_profesionales` FOREIGN KEY (`id_nivel_participacion_org`) REFERENCES `niveles_participacion_org` (`id_nivel_participacion_org`),
  CONSTRAINT `id_usuario_usuarios_participaciones_org_profesionales_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participaciones_org_profesionales`
--

LOCK TABLES `participaciones_org_profesionales` WRITE;
/*!40000 ALTER TABLE `participaciones_org_profesionales` DISABLE KEYS */;
/*!40000 ALTER TABLE `participaciones_org_profesionales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participaciones_plan_estudios`
--

DROP TABLE IF EXISTS `participaciones_plan_estudios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participaciones_plan_estudios` (
  `id_participacion_plan_estudios` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_participacion_plan_estudios` varchar(200) NOT NULL,
  `fecha_participacion_plan_estudios` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_modificacion` date NOT NULL,
  `id_usuario_modifico` int(11) NOT NULL,
  PRIMARY KEY (`id_participacion_plan_estudios`),
  KEY `id_usuario_usuarios_participaciones_plan_estudios_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_participaciones_plan_estudios_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participaciones_plan_estudios`
--

LOCK TABLES `participaciones_plan_estudios` WRITE;
/*!40000 ALTER TABLE `participaciones_plan_estudios` DISABLE KEYS */;
/*!40000 ALTER TABLE `participaciones_plan_estudios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_academicos`
--

DROP TABLE IF EXISTS `productos_academicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_academicos` (
  `id_producto_academico` int(11) NOT NULL AUTO_INCREMENT,
  `numero_producto_academico` decimal(8,0) NOT NULL,
  `descripcion_prod_academico` varchar(250) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_producto_academico`),
  KEY `id_usuario_usuarios_productos_academicos_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_productos_academicos_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_academicos`
--

LOCK TABLES `productos_academicos` WRITE;
/*!40000 ALTER TABLE `productos_academicos` DISABLE KEYS */;
INSERT INTO `productos_academicos` VALUES (1,52456,'Este es un producto academico',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `productos_academicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puestos_academicos`
--

DROP TABLE IF EXISTS `puestos_academicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puestos_academicos` (
  `id_puesto_academico` int(11) NOT NULL AUTO_INCREMENT,
  `actividad_puesto` varchar(40) NOT NULL,
  `institucion_puesto` varchar(40) NOT NULL,
  `fecha_inicio_puesto` date NOT NULL,
  `fecha_fin_puesto` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_usuario_modifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_puesto_academico`),
  KEY `id_usuario_usuarios_puestos_academicos_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_puestos_academicos_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puestos_academicos`
--

LOCK TABLES `puestos_academicos` WRITE;
/*!40000 ALTER TABLE `puestos_academicos` DISABLE KEYS */;
INSERT INTO `puestos_academicos` VALUES (1,'Docente','ITESM','2010-09-25','2015-08-12',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `puestos_academicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reconocimientos`
--

DROP TABLE IF EXISTS `reconocimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reconocimientos` (
  `id_reconocimiento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_reconocimiento` varchar(250) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_modificacion` date NOT NULL,
  `id_usuario_modifico` int(11) NOT NULL,
  PRIMARY KEY (`id_reconocimiento`),
  KEY `id_usuario_usuarios_reconocimientos_fk` (`id_usuario`),
  CONSTRAINT `id_usuario_usuarios_reconocimientos_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reconocimientos`
--

LOCK TABLES `reconocimientos` WRITE;
/*!40000 ALTER TABLE `reconocimientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `reconocimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_cursos`
--

DROP TABLE IF EXISTS `tipos_cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_cursos` (
  `id_tipo_curso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_curso` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipo_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_cursos`
--

LOCK TABLES `tipos_cursos` WRITE;
/*!40000 ALTER TABLE `tipos_cursos` DISABLE KEYS */;
INSERT INTO `tipos_cursos` VALUES (1,'Licenciatura'),(2,'Postgrado'),(3,'Educación Continua'),(4,'Otro');
/*!40000 ALTER TABLE `tipos_cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_formacion_academica`
--

DROP TABLE IF EXISTS `tipos_formacion_academica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_formacion_academica` (
  `id_tipo_formacion_academica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_formacion_academica` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipo_formacion_academica`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_formacion_academica`
--

LOCK TABLES `tipos_formacion_academica` WRITE;
/*!40000 ALTER TABLE `tipos_formacion_academica` DISABLE KEYS */;
INSERT INTO `tipos_formacion_academica` VALUES (1,'Licenciatura'),(2,'Especialización'),(3,'Maestría'),(4,'Doctorado');
/*!40000 ALTER TABLE `tipos_formacion_academica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_localidad`
--

DROP TABLE IF EXISTS `tipos_localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_localidad` (
  `id_tipo_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_tipo_localidad` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tipo_localidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_localidad`
--

LOCK TABLES `tipos_localidad` WRITE;
/*!40000 ALTER TABLE `tipos_localidad` DISABLE KEYS */;
INSERT INTO `tipos_localidad` VALUES (1,'Misma IES donde se ofrece el PE que está siendo evaluado'),(2,'En una IES ubicada en la misma localidad donde se ofrece el PE que está siendo evaluado'),(3,'En una IES del mismo estado donde se ofrece el PE que está siendo evaluado'),(4,'En una IES de otro estado en México'),(5,'En una IES del extranjero');
/*!40000 ALTER TABLE `tipos_localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_usuario`
--

DROP TABLE IF EXISTS `tipos_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_usuario` (
  `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_usuario` varchar(20) NOT NULL,
  `descripcion_tipo_usuario` varchar(250) NOT NULL,
  PRIMARY KEY (`id_tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_usuario`
--

LOCK TABLES `tipos_usuario` WRITE;
/*!40000 ALTER TABLE `tipos_usuario` DISABLE KEYS */;
INSERT INTO `tipos_usuario` VALUES (1,'Administrador','Es un usuario administrativo'),(2,'Regular','Es un usuario regular');
/*!40000 ALTER TABLE `tipos_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(50) NOT NULL,
  `contrasenia` varchar(24) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_baja` date NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_tipo_usuario_tipos_usuario_usuarios_fk` (`id_tipo_usuario`),
  CONSTRAINT `id_tipo_usuario_tipos_usuario_usuarios_fk` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Pedro','1234','2014-03-20','2014-05-06',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-10  0:38:29
