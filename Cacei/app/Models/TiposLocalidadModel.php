<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FormacionAcademicaModel;

class TiposLocalidadModel extends Model{
    protected $table = 'tipos_localidad';
    protected $primaryKey = 'id_tipo_localidad';
    public $timestamps = false;

    public function formacionesAcademicas(){
    	return $this->hasMany(FormacionAcademicaModel::class,
    		'id_tipo_localidad','id_tipo_localidad');
    }
}
