<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ExperienciaDisenioIngenierilModel;

class NivelesExperienciaIngenierilModel extends Model{
    protected $table = 'niveles_experiencia_ingenieril';
    protected $primaryKey = 'id_nivel_experiencia_ingenieril';
    public $timestamps = false;

    public function experienciasIngenieril(){
    	return $this->hasMany(ExperienciaDisenioIngenierilModel::class,
    		'id_nivel_experiencia_ingenieril','id_nivel_experiencia_ingenieril');
    }
}
