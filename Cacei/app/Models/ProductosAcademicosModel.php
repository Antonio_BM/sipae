<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductosAcademicosModel extends Model{
	protected $table = "productos_academicos";
    protected $primaryKey = "id_producto_academico";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"numero_producto_academico",
    	"descripcion_prod_academico",
    	"id_usuario"
    ];
    protected $visible = [
    	"id_producto_academico",
    	"descripcion_prod_academico"
    ];
}
