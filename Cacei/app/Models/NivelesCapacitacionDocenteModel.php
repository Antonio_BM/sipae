<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CapacitacionesDocentesModel;

class NivelesCapacitacionDocenteModel extends Model{
    protected $table = 'niveles_capacitacion_docente';
    protected $primaryKey = 'id_nivel_capacitacion_docente';
    public $timestamps = false;

    public function capacitacionesDocentes(){
    	return $this->hasMany(CapacitacionesDocentesModel::class,
    		'id_nivel_capacitacion_docente','id_nivel_capacitacion_docente');
    }
}
