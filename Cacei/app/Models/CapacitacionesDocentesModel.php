<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PaisesModel;
use App\Models\NivelesCapacitacionDocenteModel;

class CapacitacionesDocentesModel extends Model{
	protected $table = "capacitaciones_docentes";
	protected $primaryKey = "id_capacitacion_docente";
	const CREATED_AT = "fecha_registro";
	const UPDATED_AT = "fecha_modificacion";
	protected $fillable = [
		"tipo_capacitacion",             
		"institucion_capacitacion",      
		"id_pais",     
		"fecha_capacitacion",           
		"horas_capacitacion",           
		"id_nivel_capacitacion_docente", 
		"id_usuario"
	];
	protected $visible = [
		"id_capacitacion_docente",
		"tipo_capacitacion",
		"institucion_capacitacion",
		"nombre_pais",
		"fecha_capacitacion",
		"horas_capacitacion",
		"nombre_nivel_capacitacion_docente"
	];

	public function pais(){
		return $this->belongsTo(PaisesModel::class,'id_pais');
	}

	public function nivelCapacitacionDocente(){
		return $this->belongsTo(NivelesCapacitacionDocenteModel::class,
			'id_nivel_capacitacion_docente');
	}
}
