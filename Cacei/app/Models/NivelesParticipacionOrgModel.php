<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ParticipacionesOrgProfesionalesModel;

class NivelesParticipacionOrgModel extends Model{
    protected $table = 'niveles_participacion_org';
    protected $primaryKey = 'id_nivel_participacion_org';
    public $timestamps = false;

    public function participacionesOrgProfesionales(){
    	return $this->hasMany(ParticipacionesOrgProfesionalesModel::class,
    		'id_nivel_participacion_org','id_nivel_participacion_org');
    }
}
