<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformacionGeneralModel extends Model{
    protected $primaryKey = "id_informacion_general";
    protected $table = "informacion_general";
    const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
    protected $fillable = [
    	"numero_profesor",
    	"nombres_profesor",
    	"apellido_paterno",
    	"apellido_materno",
    	"fecha_nacimiento",
    	"puesto_en_institucion",
    	"fecha_contratacion_institucion",
    	"categoria_contratacion_actual",
    	"tipo_contratacion_actual",
    	"id_usuario"
    ];
    protected $hidden = [
        'id_usuario',
        'fecha_registro',
        'fecha_modificacion',
        'id_usuario_modifico'
    ];
}
