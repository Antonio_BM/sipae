<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TiposFormacionAcademicaModel;
use App\Models\PaisesModel;
use App\Models\TiposLocalidadModel;

class FormacionAcademicaModel extends Model{
	protected $table = "formacion_academica";
	protected $primaryKey = "id_formacion_academica";
	const CREATED_AT = "fecha_registro";
    const UPDATED_AT = "fecha_modificacion";
	protected $fillable = [
		'nombre_nivel_academico',
		'institucion_formacion',
		'id_pais',
		'estado_formacion',
		'ciudad_formacion',
		'fecha_formacion',
		'cedula_profesional',
		'id_tipo_formacion_academica',
		'id_tipo_localidad',
		'id_usuario'
	];
	protected $visible = [
		'id_formacion_academica',
		'nombre_nivel_academico',
		'institucion_formacion',
		'estado_formacion',
		'ciudad_formacion',
		'fecha_formacion',
		'cedula_profesional',
		'nombre_tipo_formacion_academica',
		'nombre_pais',
		'descripcion_tipo_localidad'
	];

	public function tipoFormacion(){
		return $this->belongsTo(TiposFormacionAcademicaModel::class,
			'id_tipo_formacion_academica');
	}

	public function pais(){
		return $this->belongsTo(PaisesModel::class,'id_pais');
	}

	public function tipoLocalidad(){
		return $this->belongsTo(TiposLocalidadModel::class,'id_tipo_localidad');
	}
}
