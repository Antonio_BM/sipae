<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CapacitacionesDocentesModel;
use App\Models\PaisesModel;
use App\Models\NivelesCapacitacionDocenteModel;
use Validator;

class CapacitacionesDocentes extends Controller{
    public function obtenerCatalogos(){
    	$paises = PaisesModel::pluck('nombre_pais');
    	$niveles_capacitacion = NivelesCapacitacionDocenteModel::
            pluck('nombre_nivel_capacitacion_docente');
        $capacitaciones = CapacitacionesDocentesModel::get();
        foreach ($capacitaciones as $key => $capacitacion) {
            $capacitaciones[$key]['nombre_pais'] = $capacitacion->pais->nombre_pais;
            $capacitaciones[$key]['nombre_nivel_capacitacion_docente'] = $capacitacion
                ->nivelCapacitacionDocente->nombre_nivel_capacitacion_docente;
        }
    	$respuesta = [
    		"paises" => $paises,
    		"niveles_capacitacion_docente" => $niveles_capacitacion,
            "registros" => $capacitaciones
    	];
    	return response()->json($respuesta,200);
    }

    public function insertarCapacitacion(Request $req){
    	$reglas = [
    		"tipo_capacitacion" => "required",             
			"institucion_capacitacion" => "required",      
			"nombre_pais" => "required",     
			"fecha_capacitacion" => "required",           
			"horas_capacitacion" => "required",           
			"nombre_nivel_capacitacion_docente" => "required", 
			"id_usuario" => "required"
    	];
        $capacitacion = $req->json()->all();
    	$validacion = Validator::make($capacitacion,$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la información"],400);
    	}
        $capacitacion['id_pais'] = DB::table('paises')
            ->where('nombre_pais',$capacitacion['nombre_pais'])->value('id_pais');
        $capacitacion['id_nivel_capacitacion_docente'] = 
            DB::table('niveles_capacitacion_docente')
            ->where('nombre_nivel_capacitacion_docente',
                $capacitacion['nombre_nivel_capacitacion_docente'])
            ->value('id_nivel_capacitacion_docente');
    	$datos = CapacitacionesDocentesModel::create($capacitacion);
        $respueta = [
            "mensaje" => "se registró correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarCapacitacionDocente(Request $req, 
        CapacitacionesDocentesModel $capacitacion){
        $capacitacion->delete();
        return response()->json(null,204);
    }

    public function actualizarCapacitacionDocente(Request $req, 
        CapacitacionesDocentesModel $capacitacion){
        $datos = $req->json()->all();
        $pais = PaisesModel::where('nombre_pais',$datos['nombre_pais'])->first();
        $nivel = NivelesCapacitacionDocenteModel::
        where('nombre_nivel_capacitacion_docente',
            $datos['nombre_nivel_capacitacion_docente'])->first();
        $datos['id_pais'] = $pais->id_pais;
        $datos['id_nivel_capacitacion_docente'] = $nivel->id_nivel_capacitacion_docente;
        $capacitacion->update($datos);
        return response()->json($capacitacion,200);
    }
}
