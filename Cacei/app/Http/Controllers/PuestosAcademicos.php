<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PuestosAcademicosModel;
use Validator;

class PuestosAcademicos extends Controller{

    public function obtenerCatalogos(){
        $puestos = PuestosAcademicosModel::get();
        $respuesta = [
            "registros" => $puestos
        ];
        return response()->json($respuesta,200);
    }

    public function insertarPuestoAcademico(Request $req){
    	$reglas = [
    		"actividad_puesto" => "required",
	    	"institucion_puesto" => "required",
	    	"fecha_inicio_puesto" => "required",
	    	"fecha_fin_puesto" => "required",
	    	"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la informacion"],400);
    	}
    	$datos = PuestosAcademicosModel::create($req->json()->all());
        $respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarPuestoAcademico(Request $req, PuestosAcademicosModel $puesto){
        $puesto->delete();
        return response()->json(null,204);
    }

    public function actualizarPuestoAcademico(Request $req, 
        PuestosAcademicosModel $puesto){
        $puesto->update($req->json()->all());
        return response()->json($puesto,200);
    }
}
