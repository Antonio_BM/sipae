<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReconocimientosModel;
use Validator;

class Reconocimientos extends Controller{

    public function obtenerCatalogos(){
        $reconocimientos = ReconocimientosModel::get();
        $respuesta = [
            "registros" => $reconocimientos
        ];
        return response()->json($respuesta,200);
    }

    public function insertarReconocimiento(Request $req){
    	$reglas = [
    		"descripcion_reconocimiento" => "required",
    		"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al cargar la informacion"],400);
    	}
    	$datos = ReconocimientosModel::create($req->json()->all());
        $respuesta = [
            "mensaje" => "Se insertó correctamente la información"
        ];
    	return response()->json($respuesta,201);
    }
    public function eliminarReconocimiento(Request $req, 
        ReconocimientosModel $reconocimiento){
        $reconocimiento->delete();
        return response()->json(null,204);
    }

    public function actualizarReconocimiento(Request $req, 
        ReconocimientosModel $reconocimiento){
        $reconocimiento->update($req->json()->all());
        return response()->json($reconocimiento,200);
    }
}
