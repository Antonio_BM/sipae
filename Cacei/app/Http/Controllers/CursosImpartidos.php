<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CursosImpartidosModel;
use App\Models\ClavesCursosModel;
use App\Models\TiposCursosModel;
use Validator;

class CursosImpartidos extends Controller{
    public function obtenerCatalogos(){
    	$claves_cursos = ClavesCursosModel::pluck("clave_curso");
    	$nombres_tipos_cursos = TiposCursosModel::pluck("nombre_tipo_curso");
        $cursos = CursosImpartidosModel::get();
        foreach ($cursos as $key => $curso) {
            $cursos[$key]['clave_curso'] = $curso->claveCurso->clave_curso;
            $cursos[$key]['nombre_tipo_curso'] = $curso->tipoCurso->nombre_tipo_curso;
        }
    	$respuesta = [
    		"claves_cursos" => $claves_cursos,
    		"nombres_tipos_cursos" => $nombres_tipos_cursos,
            "registros" => $cursos
    	];
    	return response()->json($respuesta,200);
    }

    public function insertarCursoImpartido(Request $req){
    	$reglas = [
    		"nombre_curso_impartido" => "required",
	    	"fecha_inicio_curso_impartido" => "required",
	    	"fecha_fin_curso_impartido" => "required",
	    	"clave_curso" => "required",
	    	"nombre_tipo_curso" => "required",
	    	"horas_curso_impartido" => "required",
	    	"evaluacion_desempenio" => "required",
	    	"id_usuario" => "required"
    	];
        $curso = $req->json()->all();
    	$validacion = Validator::make($curso,$reglas);
    	if($validacion->fails()){
    		return response()->json("Error al cargar la información",400);
    	}
        $curso['id_tipo_curso'] = DB::table('tipos_cursos')
            ->where('nombre_tipo_curso',$curso['nombre_tipo_curso'])
            ->value('id_tipo_curso');
        $curso['id_clave_curso'] = DB::table('claves_cursos')
            ->where('clave_curso',$curso['clave_curso'])
            ->value('id_clave_curso');
    	$datos = CursosImpartidosModel::create($curso);
        $respuesta = [
            "mensaje" => "Se agregó correctamente"
        ];
    	return response()->json($respuesta,201);
    }

    public function eliminarCursoImpartido(Request $req, CursosImpartidosModel $curso){
        $curso->delete();
        return response()->json(null,204);
    }

    public function actualizarCursoImpartido(Request $req, CursosImpartidosModel $curso){
        $datos = $req->json()->all();
        $clave = ClavesCursosModel::where('clave_curso',$datos['clave_curso'])->first();
        $tipo_curso = TiposCursosModel::where('nombre_tipo_curso',
            $datos['nombre_tipo_curso'])->first();
        $datos['id_clave_curso'] = $clave->id_clave_curso;
        $datos['id_tipo_curso'] = $tipo_curso->id_tipo_curso;
        $curso->update($datos);
        return response()->json($curso,200);
    }
}
