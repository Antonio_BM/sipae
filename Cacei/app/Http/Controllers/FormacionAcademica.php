<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormacionAcademicaModel;
use App\Models\PaisesModel;
use App\Models\TiposFormacionAcademicaModel;
use App\Models\TiposLocalidadModel;
use Illuminate\Support\Facades\DB;
use Validator;

class FormacionAcademica extends Controller{
	public function obtenerCatalogos(){
		$paises = PaisesModel::pluck('nombre_pais');
		$formaciones = TiposFormacionAcademicaModel::
			pluck('nombre_tipo_formacion_academica');
		$descripcion_tipo_localidad = TiposLocalidadModel::
			pluck('descripcion_tipo_localidad');
		$registroFormaciones = FormacionAcademicaModel::get();
		foreach ($registroFormaciones as $key => $registroFormacion) {
			$registroFormaciones[$key]['nombre_tipo_formacion_academica'] 
				= $registroFormacion->tipoFormacion->nombre_tipo_formacion_academica;
			$registroFormaciones[$key]['nombre_pais'] 
				= $registroFormacion->pais->nombre_pais;
			$registroFormaciones[$key]['descripcion_tipo_localidad'] 
				= $registroFormacion->tipoLocalidad->descripcion_tipo_localidad;
		}
		$respuesta = [
			"paises" => $paises,
			"nombres_tipos_formacion_academica"=> $formaciones,
			"descripciones_tipos_localidad"=>$descripcion_tipo_localidad,
			"registros" => $registroFormaciones
		];

		return response()->json($respuesta,200);
	}

	public function insertarFormacionAcademica(Request $req){
		$reglas = [
			'nombre_nivel_academico' => 'required',
			'institucion_formacion' => 'required',
			'nombre_pais' => 'required',
			'estado_formacion' => 'required',
			'ciudad_formacion' => 'required',
			'fecha_formacion' => 'required',
			'cedula_profesional' => 'required',
			'nombre_tipo_formacion_academica' => 'required',
			'descripcion_tipo_localidad' => 'required',
			'id_usuario' => 'required'
		];
		$formacion = $req->json()->all();
		$validacion = Validator::make($formacion,$reglas);
		if($validacion->fails()){
			return response()->json(["mensaje" => "Error al cargar la información"],400);
		}
		$formacion['id_pais'] = DB::table("paises")
			->where("nombre_pais",$formacion["nombre_pais"])
			->value("id_pais");
		$formacion['id_tipo_formacion_academica'] = DB::table("tipos_formacion_academica")
			->where("nombre_tipo_formacion_academica",
				$formacion["nombre_tipo_formacion_academica"])
			->value("id_tipo_formacion_academica");
		$formacion['id_tipo_localidad'] = DB::table("tipos_localidad")
			->where("descripcion_tipo_localidad",$formacion["descripcion_tipo_localidad"])
			->value("id_tipo_localidad");
		$datos = FormacionAcademicaModel::create($formacion);
		$respuesta = [
			"mensaje" => "Se registro correctamente"
		];
		return response()->json($respuesta,201);
	}

	public function eliminarFormacionAcademica(Request $req, 
		FormacionAcademicaModel $formacion){
		$formacion->delete();
		return response()->json(null,204);
	}

	public function actualizarFormacionAcademica(Request $req, 
		FormacionAcademicaModel $formacion){
		$datos = $req->json()->all();
		$pais = PaisesModel::where('nombre_pais',$datos['nombre_pais'])->first();
		$tipo_formacion = TiposFormacionAcademicaModel::
		where('nombre_tipo_formacion_academica',
			$datos['nombre_tipo_formacion_academica'])->first();
		$tipo_localidad = TiposLocalidadModel::where('descripcion_tipo_localidad',
			$datos['descripcion_tipo_localidad'])->first();
		$datos['id_pais'] = $pais->id_pais;
		$datos['id_tipo_formacion_academica'] = 
			$tipo_formacion->id_tipo_formacion_academica;
		$datos['id_tipo_localidad'] = $tipo_localidad->id_tipo_localidad;
		$formacion->update($datos);
		return response()->json($formacion,200);
	}
}
