<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExperienciaProfesionalModel;
use Validator;

class ExperienciaProfesional extends Controller{

    public function obtenerCatalogos(){
        $experiencias = ExperienciaProfesionalModel::get();
        $respuesta = [
            "registros" => $experiencias
        ];
        return response()->json($respuesta,200);
    }

    public function insertarExperienciaProfesional(Request $req){
    	$reglas = [
    		"actividad_puesto_experiencia" => "required",
	    	"empresa_experiencia" => "required",
	    	"fecha_inicio_experiencia" => "required",
	    	"fecha_fin_experiencia" => "required",
	    	"id_usuario" => "required"
    	];
    	$validacion = Validator::make($req->json()->all(),$reglas);
    	if($validacion->fails()){
    		return response()->json(["mensaje" => "Error al subir la información"],400);
    	}
    	$datos = ExperienciaProfesionalModel::create($req->json()->all());
        $respuesta = [
            "mensaje" => "Se registro correctamente"
        ];
    	return response()->json($respuesta,201); 
    }
    public function eliminarExperienciaProfesional(Request $req, 
        ExperienciaProfesionalModel $experiencia){
        $experiencia->delete();
        return response()->json(null,204);
    }

    public function actualizarExperienciaProfesional(Request $req, 
        ExperienciaProfesionalModel $experiencia){
        $experiencia->update($req->json()->all());
        return response()->json($experiencia,200);
    }
}
