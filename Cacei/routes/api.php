<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','Login@login');
Route::post('login/verificarsesion','Login@verificarSesion');
Route::post('login/logout','Login@logOut');

Route::post('informaciongeneral','InformacionGeneral@informacionGeneral');

Route::get('formacionacademica','FormacionAcademica@obtenerCatalogos');
Route::post('formacionacademica','FormacionAcademica@insertarFormacionAcademica');
Route::delete('formacionacademica/{formacion}',
	'FormacionAcademica@eliminarFormacionAcademica');
Route::put('formacionacademica/{formacion}',
	'FormacionAcademica@actualizarFormacionAcademica');

Route::get('actualizaciondisciplinar','ActualizacionesDisciplinares@obtenerCatalogos');
Route::post('actualizaciondisciplinar',
	'ActualizacionesDisciplinares@insertarActualizacion');
Route::delete('actualizaciondisciplinar/{actualizacion}',
	'ActualizacionesDisciplinares@eliminarActualizacion');
Route::put('actualizaciondisciplinar/{actualizacion}',
	'ActualizacionesDisciplinares@actualizarActualizacion');

Route::get('capacitaciondocentes','CapacitacionesDocentes@obtenerCatalogos');
Route::post('capacitaciondocentes','CapacitacionesDocentes@insertarCapacitacion');
Route::delete('capacitaciondocentes/{capacitacion}',
	'CapacitacionesDocentes@eliminarCapacitacionDocente');
Route::put('capacitaciondocentes/{capacitacion}',
	'CapacitacionesDocentes@actualizarCapacitacionDocente');

Route::get('cursosimpartidos','CursosImpartidos@obtenerCatalogos');
Route::post('cursosimpartidos','CursosImpartidos@insertarCursoImpartido');
Route::delete('cursosimpartidos/{curso}','CursosImpartidos@eliminarCursoImpartido');
Route::put('cursosimpartidos/{curso}','CursosImpartidos@actualizarCursoImpartido');

Route::get('experienciadisenioingenieril',
	'ExperienciaDisenioIngenieril@obtenerCatalogos');
Route::post('experienciadisenioingenieril',
	'ExperienciaDisenioIngenieril@insertarExperienciaDisenioIngenieril');
Route::delete('experienciadisenioingenieril/{experiencia}',
	'ExperienciaDisenioIngenieril@eliminarExperienciaDisenioIngenieril');
Route::put('experienciadisenioingenieril/{experiencia}',
	'ExperienciaDisenioIngenieril@actualizarExperienciaDisenioIngenieril');


Route::get('experienciaprofesional','ExperienciaProfesional@obtenerCatalogos');
Route::post('experienciaprofesional',
	'ExperienciaProfesional@insertarExperienciaProfesional');
Route::delete('experienciaprofesional/{experiencia}',
	'ExperienciaProfesional@eliminarExperienciaProfesional');
Route::put('experienciaprofesional/{experiencia}',
	'ExperienciaProfesional@actualizarExperienciaProfesional');

Route::get('logrosprofesionales','LogrosProfesionales@obtenerCatalogos');
Route::post('logrosprofesionales','LogrosProfesionales@insertarLogroProfesional');
Route::delete('logrosprofesionales/{logro}',
	'LogrosProfesionales@eliminarLogroProfesional');
Route::put('logrosprofesionales/{logro}',
	'LogrosProfesionales@actualizarLogroProfesional');

Route::get('participacionesorgprofesionales',
	'ParticipacionesOrgProfesionales@obtenerCatalogos');
Route::post('participacionesorgprofesionales',
	'ParticipacionesOrgProfesionales@insertarParticipacionOrgProfesional');
Route::delete('participacionesorgprofesionales/{participacion}',
	'ParticipacionesOrgProfesionales@eliminarParticipacionOrgProfesional');
Route::put('participacionesorgprofesionales/{participacion}',
	'ParticipacionesOrgProfesionales@actualizarParticipacionOrgProfesional');

Route::get('participacionesplanestudios','ParticipacionesPlanEstudios@obtenerCatalogos');
Route::post('participacionesplanestudios',
	'ParticipacionesPlanEstudios@insertarParticipacionPlanEstudios');
Route::delete('participacionesplanestudios/{participacion}',
	'ParticipacionesPlanEstudios@eliminarParticipacionPlanEstudios');
Route::put('participacionesplanestudios/{participacion}',
	'ParticipacionesPlanEstudios@actualizarParticipacionPlanEstudios');

Route::get('productosacademicos','ProductosAcademicos@obtenerCatalogos');
Route::post('productosacademicos','ProductosAcademicos@insertarProductoAcademico');
Route::delete('productosacademicos/{producto}',
	'ProductosAcademicos@eliminarProductoAcademico');
Route::put('productosacademicos/{producto}',
	'ProductosAcademicos@actualizarProductoAcademico');

Route::get('puestosacademicos','PuestosAcademicos@obtenerCatalogos');
Route::post('puestosacademicos','PuestosAcademicos@insertarPuestoAcademico');
Route::delete('puestosacademicos/{puesto}','PuestosAcademicos@eliminarPuestoAcademico');
Route::put('puestosacademicos/{puesto}','PuestosAcademicos@actualizarPuestoAcademico');

Route::get('reconocimientos','Reconocimientos@obtenerCatalogos');
Route::post('reconocimientos','Reconocimientos@insertarReconocimiento');
Route::delete('reconocimientos/{reconocimiento}','Reconocimientos@eliminarReconocimiento');
Route::put('reconocimientos/{reconocimiento}','Reconocimientos@actualizarReconocimiento');

Route::get('exportardatos','ExportarDatos@obtenerDatos');

